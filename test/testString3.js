const monthName = require("../string3");

console.log(monthName("10/1/2020"))
console.log(monthName("Hello World"))
console.log(monthName("14/1/2021"))
console.log(monthName("10/38/2021"))
console.log(monthName(26262))
console.log(monthName())
console.log(monthName([]))
console.log(monthName(["10/1/2021"]))
console.log(monthName("11/1/2021"))
console.log(monthName("2/29/2021"))
console.log(monthName("2/29/2016"))
