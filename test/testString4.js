const nameExtractor = require("../string4");

console.log(nameExtractor({"first_name": "JoHN", "last_name": "SMith"}))
console.log(nameExtractor({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}))
console.log(nameExtractor("first_name"))
console.log(nameExtractor())
console.log(nameExtractor([]))
console.log(nameExtractor(["first_name", "JoHN", "middle_name", "doe", "last_name", "SMith"]))
console.log(nameExtractor({}))