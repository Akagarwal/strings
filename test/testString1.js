let strToNum = require('../string1')

console.log(strToNum("$100.45"));
console.log(strToNum("100.45"));
console.log(strToNum("-$100.45"));
console.log(strToNum(6600));
console.log(strToNum());
console.log(strToNum([6600]));
console.log(strToNum(["$100.45"]));
console.log(strToNum([]));
console.log(strToNum("hey there"));