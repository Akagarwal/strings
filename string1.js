function strToNum(value)
{
    if (typeof value === 'string')
    {
        let output = value.replace("$",'');
        if (output === value)
        {
            return 0;
        }
        
        output = parseFloat(output);
        if (isNaN(output))
        {
            return 0;
        }
        return (output);
    }
    else
    {
        return 0;
    }
}

module.exports = strToNum;
