function nameExtractor(value)
{
    if (typeof value === 'object' && value != '')
    {
        let name = ''
        for (let i in value)
        {
            let str = value[i].toLowerCase();
            if (str === "first_name")
            {
                return "Not Valid Object"
            }
            let val = str[0].toUpperCase();
            let ans = val + str.substring(1)
            name = name + " " + ans
        }
        if (name.trim() === '')
        {
            return "Not Valid Object"
        }
        return name.trim()
    }
    else
    {
        return "Not Valid Object"
    }    
}

module.exports = nameExtractor;
